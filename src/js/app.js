"use strict";

const mobileMenu = document.querySelector('.navbar-items-container');
const mobileBtn = document.querySelector('.header__navbar__btn');
const openMobileMenu = document.querySelector('.fa-bars');
const closeMobileMenu = document.querySelector('.fa-times');

function dropMenu() {
    mobileMenu.classList.toggle('display');
    openMobileMenu.classList.toggle('display');
    closeMobileMenu.classList.toggle('display');
}


mobileBtn.addEventListener('click', dropMenu);
